// //ES5
// var firstName = 'CyberSoft';
// //Cho phép gán lại gtri
// firstName = 'CyberSoft 1';
// //Cho phép khai báo trùng tên biến
// var firstName = 'CyberSoft2';

// console.log(firstName);

// //ES6
// let seconName = 'CyberSoft new';
// //Cho phép gán lại gtri
// seconName = 'CyberSoft new 2';
// //Không cho phép khai báo trùng tên biến

// // const
// const PI = 3.14;
// //Không được phép gán giá trị
// // PI = 3.1414;
// //Không được khai báo trùng tên

//Arrow function
//ES5
// let hello = function(name) {
//     return "Hello" + name;
// }

// console.log(hello("bé mỡ"));

//ES6
// let hello1 = (name) => {
//     return "Hello1" + name;
// }

// console.log(hello1("Bé heo"));


//Chỉ có 1 tham số,có thể bỏ dấu ()
// let hello1 = name => {
//     return "Hello1" + name;
// }

// console.log(hello1("Bé heo"));


//Nếu chỉ có 1 lệnh return, thì bỏ dấu {} và từ khóa return
// let hello1 = name => "Hello1" + name;


// console.log(hello1("Bé heo"));

//Lỗi cú pháp


//Con trỏ This
//ES5
// let hocVien = {
//     hoTen: "Nguyễn Thị Học Viên",
//     lop: "Ngữ văn 12",
//     diemThi: [10, 9, 8],
//     // layThongTinHocVien: function() {
//     //     //C1: Biến tạm
//     //     //this là hocVien
//     //     //_bind là hocVien
//     //     var _bind = this;
//     //     this.diemThi.map(function(diem, index) {
//     //         //context
//     //         console.log("Họ tên: " + _bind.hoTen + " - Lớp " + _bind.lop);
//     //         console.log("Điểm thi " + index + ": " + diem);
//     //     });
//     // }
//     layThongTinHocVien: function() {
//         //C2: sử dụng hàm bind() để định nghĩa lại ngữ cảnh của this
//         this.diemThi.map(function(diem, index) {
//             //context
//             console.log("Họ tên: " + this.hoTen + " - Lớp " + this.lop);
//             console.log("Điểm thi " + index + ": " + diem);
//         }.bind(this));
//     }
// }

// hocVien.layThongTinHocVien();


//Con trỏ This
//ES6
// let hocVien = {
//     hoTen: "Nguyễn Thị Học Viên",
//     lop: "Ngữ văn 12",
//     diemThi: [10, 9, 8],
//     layThongTinHocVien: function() {
//         this.diemThi.map((diem, index) => {
//             //context
//             console.log("Họ tên: " + this.hoTen + " - Lớp " + this.lop);
//             console.log("Điểm thi " + index + ": " + diem);
//         });
//     }
// }

// hocVien.layThongTinHocVien();

//ES5
// function getUserInfo(name, age) {
//     //C1 : dùng if else
//     // if (name == undefined || age == undefined) {
//     //     console.log("Giá trị không hợp lệ!");
//     //     name = "default name";
//     //     age = 18;
//     // }

//     //C2 : ternary operator
//     // name = name == undefined ? "default name" : name;
//     // age = age == undefined ? 18 : age;

//     //C3 : toán tử ||
//     name = name || "default name";
//     age = age || 30;

//     if (age >= 18 && age < 30) {
//         console.log(name + " đang còn trẻ." + name + " nên đi chơi nhiều.")
//     } else {
//         console.log(name + " đã " + age + " tuổi rồi.Nên nghỉ ngơi!")
//     }
// }

// getUserInfo();

//ES6
// let getUserInfo = (name = "Quyền", age = 18) => {
//     if (age >= 18 && age < 30) {
//         console.log(name + " đang còn trẻ." + name + " nên đi chơi nhiều.")
//     } else {
//         console.log(name + " đã " + age + " tuổi rồi.Nên nghỉ ngơi!")
//     }
// }

// getUserInfo();

//Rest Parameter

// let tinhDTB = (...danhSachDiem) => {
//     console.log(danhSachDiem);
//     let tongDiem = 0;
//     for (let i = 0; i < danhSachDiem.length; i++) {
//         tongDiem += danhSachDiem[i];
//     }
//     console.log(tongDiem);
//     let dtb = tongDiem / danhSachDiem.length;
//     console.log(dtb);
// }

// tinhDTB(9, 8, 10);

//Spread Operator

//Thêm phần tử vào mảng
// let mangC = [1, 2, 3, 4];
// let mangD = [...mangC];

// mangD.push(5);
// mangD.push(6);

// console.log(mangD);

//Thêm thuộc tính cho đối tượng
// let pet = {
//     name: "Bé Na",
//     bread: "Snake"
// };

// let newPet = {
//     age: 1,
//     ...pet
// }

// console.log(newPet);

//Destrutureing

//Array
// let programs = ['JavaScript', 'Java', 'Python'];

//ES5
// console.log(programs[0]);
// console.log(programs[1]);
// console.log(programs[2]);

//ES6
// let [first, second, third] = programs;
// console.log(first);
// console.log(second);
// console.log(third);

//Object
// let pet = {
//     name: 'Gâu Đần',
//     age: 3,
//     break: 'Golden',
//     size: {
//         weight: '30kg',
//         height: '56cm'
//     }
// }

//ES5
// var name = pet.name;
// var age = pet.age;
// var size = pet.size;
// console.log(name, age);
// console.log(size);

//ES6
// let { name, age } = pet;
// let { weight, height } = pet.size;
// // console.log(name, age);
// // console.log(weight, height);
// let { weight: w, height: h } = pet.size;
// console.log(w, h);

//Template String
// let pet = "Cá";
// let action = "bơi";

//Mình là cá, việc của mình là bơi
//ES5

// console.log("Mình là " + pet + ", " +
//     "việc của mình là " + action + ".");

//ES6
// console.log(`Mình là ${pet}, việc của mình là ${action}.`)

// document.getElementById("section1").innerHTML = `
//     <div class='class1'>
//         <p> Hello các bạn <br>
//         Mình là Quyền nè!
//         </p>
//     </div>
// `;

//Object literal
//ES5
// var name = "Mị";
// var myObject = {
//     name: name,
//     sayHi: function() {
//         console.log("hi, my name is " + this.name);
//     }
// }
// myObject.sayHi();

//ES6
// let name = "Mị";
// let myObject = {
//     name,
//     sayHi() {
//         console.log("hi, my name is " + this.name);
//     }
// }
// myObject.sayHi();

//for
// let currencies = ['VND', 'USD', 'JPY'];
//index
// for (let i = 0; i < currencies.length; i++) {
//     console.log(i, currencies[i]);
// }

//ES5
//for ...in
// for (let i in currencies) {
//     console.log(i, currencies[i]);
// }

//ES6 
//for ...of
//value
// for (let value of currencies) {
//     console.log(value);
// }

//Lấy index
// for (let [index, value] of currencies.entries()) {
//     console.log(index, value);
// }