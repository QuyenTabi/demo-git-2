// ES5
// console.log(sum(5, 2));

//Node.js
// var callSum = require('./sum.js');

// console.log("Cộng 2 số: " + callSum(5, 3));

//ES6
// import callSum from './sum.js';
// console.log("Cộng 2 số: " + callSum(5, 3));

// import { sum as callSum } from './sum.js';
// console.log("Cộng 2 số: " + callSum(5, 3));

// import { sum, sum2 } from "./sum.js";
// console.log("Cộng 2 số: " + sum(5, 3));
// console.log("Cộng 3 số: " + sum2(5, 3, 2));

import * as sumFunctions from './sum.js';
console.log("Cộng 2 số: " + sumFunctions.sum(5, 3));
console.log("Cộng 3 số: " + sumFunctions.sum2(5, 3, 3));